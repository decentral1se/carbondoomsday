### Current Status

Unfortunately, the team has moved on from this project and no further
development will be taking place unless more people step up to resurrect the
project. Therefore, we probably won't be accepting pull requests for anything
beyond maintenance issues for now. As per usual, please come and talk to us on
the Gitter chat room if you have ideas.

---

[![pipeline status](https://git.coop/lwm/carbondoomsday/badges/master/pipeline.svg)](https://git.coop/lwm/carbondoomsday/commits/master)
[![coverage report](https://git.coop/lwm/carbondoomsday/badges/master/coverage.svg)](https://git.coop/lwm/carbondoomsday/commits/master)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](http://www.gnu.org/licenses/agpl-3.0)

[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)
[![Gitter chat](https://badges.gitter.im/giving-a-fuck-about-climate-change/gitter.png)](https://gitter.im/giving-a-fuck-about-climate-change/Lobby)

![CarbonDoomsDay Logo](https://i.imgur.com/jfj3CMs.png)

A [Django] web API for climate change related data inspired by [no one giving a fuck].

[Django]: https://www.djangoproject.com/
[no one giving a fuck]: http://titojankowski.com/no-one-gives-a-fck-about-climate-change/

From [Carbon Doomsday: Tracking CO2 since 1958]:

[Carbon Doomsday: Tracking CO2 since 1958]: http://datadrivenjournalism.net/featured_projects/carbon_doomsday_tracking_co2_since_1958

> Carbon Doomsday is a real-time API and chart of worldwide carbon dioxide
> levels. Developed as a community project, its goal is to be an open-source
> platform for climate data. Data for the API and chart comes from NOAA’s Earth
> System Research Lab in Mauna Loa, Hawaii. The project is rooted in principles
> of free software, open data access and a willingness to contribute to further
> education on the global climate issue.

Check out [carbondoomsday.com], (powered by [carbon-inferno]) our CO2 emissions tracker, which consumes this web API.

[carbondoomsday.com]: http://carbondoomsday.com/
[carbon-inferno]: https://git.coop/lwm/carbon-inferno

# Use the API

Please see the [Swagger API documentation] to get started.

[Swagger API documentation]: http://api.carbondoomsday.com/

# Contribute

This is a community driven project which aims to bring all levels of
contributor to engage in providing a reliable, [free] and feature packed web
API.

[free]: https://fsfe.org/about/basics/freesoftware.en.html

If you're interested in contributing, we need you! Please check out the following:

  * [Contribution Documentation]: Contribute some source code!
  * [Gitter Lobby]: Come chat to us!

[Contribution Documentation]: https://git.coop/lwm/carbondoomsday/blob/master/CONTRIBUTING.md
[Gitter Lobby]: https://gitter.im/giving-a-fuck-about-climate-change/Lobby
