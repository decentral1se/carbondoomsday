"""Command to generate the email signature."""

from django.core.management.base import BaseCommand

from carbondoomsday.email_signature import tasks


class Command(BaseCommand):
    help = 'Scrape daily MLO CO2 measurements since 1958.'

    def handle(self, *args, **options):
        tasks.update_email_signature_task.apply()
