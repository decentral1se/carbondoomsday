# Table of Contents
  * [Get a Local Copy](#get-a-local-copy)
  * [Configure the Environment](#configure-the-environment)
  * [Install Python Dependencies](#install-python-dependencies)
  * [Install System Dependencies](#install-system-dependencies)
  * [Populate Database](#populate-database)
  * [Start The Application](#start-the-application)
  * [Run The Test Suite](#run-the-test-suite)
  * [Run The Full Test Suite](#run-the-full-test-suite)
  * [Making Deployments](#making-deployments)
    * [SSH Keys](#ssh-keys)
    * [Adding The Remote](#adding-the-remote)
    * [Git Deploy](#git-deploy)

# Get a Local Copy

Firstly, [fork the repository] and [git clone] it:

[fork the repository]: https://help.github.com/articles/fork-a-repo/
[git clone]: https://git-scm.com/book/en/Getting-Started-Git-Basics

``` bash
$ git clone git@github.com:<your-username>/carbondoomsday.git
```

# Install Python Dependencies

We manage our Python dependencies with [Pipenv], you can [install it via Pipsi]:


[Pipenv]: http://pipenv.org/
[install it via Pipsi]: https://pipenv.readthedocs.io/en/latest/install/#fancy-installation-of-pipenv

``` bash
$ curl https://raw.githubusercontent.com/mitsuhiko/pipsi/master/get-pipsi.py | python3
$ source ~/.bashrc  # OPTIONAL, depending on your operating system and configuration
$ pipsi install pew
$ pipsi install pipenv
```

> Pipsi is installed in a location that you may have to add to your $PATH
> environment variable. You'll have to add $HOME/.local/bin to your $PATH in
> order to get access to the `pipsi` executable

Then run:

``` bash
$ pipenv install --dev --python 3.6
```

# Configure the Environment

We use [django-configurations] to configure the application in the spirit of
the [The Twelve-Factor App]. You'll need to have a number of environment
variables to configure the application.

[django-configurations]: https://github.com/jazzband/django-configurations
[The Twelve-Factor App]: https://12factor.net/config

Luckily, [pipenv does this for us]. You just need to make a copy of the
existing [example.env], rename it as a `.env` file  and fill it with values
that are right for your environment.

[pipenv does this for us]: https://docs.pipenv.org/advanced/#automatic-loading-of-env
[the example configuration to get started]: https://github.com/giving-a-fuck-about-climate-change/carbondoomsday/blob/master/example.env

# Using Pipenv

There are many helpful commands contained in the Makefile. Each of them
make sure to run commands inside the pipenv environment. If you're not
using the targets, please remember to prefix your commands with:

```bash
$ pipenv run <command>
```

Or else, just drop into a configured environment with:

```bash
$ pipenv shell
```

# Install System Dependencies

The application relies on [PostgreSQL] and [Redis].

Please refer to the relevant installation method for your operating system.

[PostgreSQL]: https://www.postgresql.org/
[Redis]: https://redis.io/

# Populate Database

You can load the latest data into your PostgreSQL with:

``` bash
$ make dbmigrate
```

And get the latest data:

``` bash
$ make scrape_mlo_co2_since_2015
```

# Start The Application

```bash
$ make server
```

# Run the Test Suite

```bash
$ make test
```

# Run the Full Test Suite

```bash
$ make proof
```

# Making Deployments

Deployments are made automatically when any pull requests is merged.

What follows is the manual guide for managing deployments from your local machine.

## SSH Keys

You'll need to get your SSH public key part sent to:

> lukewm[at]riseup[dot]net

## Adding the Remote

Your key will be synced to the [dokku] installation.

Then, you'll need to add the remote:

```bash
$ git remote add dokku dokku@178.62.103.145:api
```

[dokku]: http://dokku.viewdocs.io/dokku/

## Git Deploy

Then, just run the usual Git based deployment with:

```bash
$ git push dokku master
```

You should see the build information streamed.
