"""WSGI configuration."""

from whitenoise.django import DjangoWhiteNoise

from configurations.wsgi import get_wsgi_application

application = get_wsgi_application()
application = DjangoWhiteNoise(application)
