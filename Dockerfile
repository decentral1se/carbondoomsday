FROM python:3.6-stretch

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

WORKDIR /app

COPY . ./

RUN apt-get update && apt-get install -y \
  build-essential \
  libjpeg-dev \
  libpq-dev \
  python3-dev \
  zlib1g \
  zlib1g-dev

RUN pip install -U pip setuptools pipenv

RUN pipenv install --deploy
